/*
 * File: app/view/MyPanel80.js
 *
 * This file was generated by Sencha Architect version 3.0.4.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Sencha Touch 2.2.x library, under independent license.
 * License of Sencha Architect does not include license for Sencha Touch 2.2.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('MyApp.view.MyPanel80', {
    extend: 'Ext.Panel',

    requires: [
        'Ext.Spacer',
        'Ext.Panel',
        'Ext.Label',
        'Ext.field.Select',
        'Ext.field.TextArea'
    ],

    config: {
        height: '100%',
        id: 'chkDataPnl1',
        itemId: 'chkDataPnl1',
        padding: '0 10 10 10',
        width: '100%',
        hideOnMaskTap: false,
        scrollable: 'vertical',
        items: [
            {
                xtype: 'spacer',
                height: 10
            },
            {
                xtype: 'panel',
                height: 60,
                width: '100%',
                layout: 'hbox',
                items: [
                    {
                        xtype: 'panel',
                        cls: 'prepText',
                        docked: 'right',
                        height: 25,
                        id: 'chekListDate3',
                        width: 100,
                        items: [
                            {
                                xtype: 'textfield',
                                centered: false,
                                cls: [
                                    'cehcklistDate2',
                                    'whiteTextDate'
                                ],
                                id: 'datePickers1',
                                itemId: 'datePicker',
                                clearIcon: false,
                                labelAlign: 'bottom',
                                labelWidth: '0%',
                                name: '',
                                placeHolder: 'Select Date',
                                readOnly: true
                            }
                        ]
                    }
                ]
            },
            {
                xtype: 'label',
                cls: 'blackFont',
                html: 'Select Deal'
            },
            {
                xtype: 'selectfield',
                cls: 'prepTextLeft',
                id: 'nwDealSelectFld1',
                label: '',
                name: 'name',
                displayField: 'name',
                store: 'deal',
                valueField: 'name'
            },
            {
                xtype: 'spacer',
                height: 10
            },
            {
                xtype: 'label',
                cls: 'blackFont',
                html: 'Checklist Name'
            },
            {
                xtype: 'textfield',
                id: 'ChkTitle1',
                itemId: 'mytextfield1',
                clearIcon: false,
                label: ''
            },
            {
                xtype: 'spacer',
                height: 10
            },
            {
                xtype: 'label',
                cls: 'blackFont',
                html: '1. Situation Summary. What are you dealing with?'
            },
            {
                xtype: 'textareafield',
                id: 'nwCheckListQ11',
                itemId: 'mytextarea10',
                clearIcon: false,
                label: '',
                listeners: [
                    {
                        fn: function(element, eOpts) {
                            /*
                            //Sencha Touch 2.0 - How to set scrolling inside a textarea for Mobile Safari?
                            handleTouch: function(e) {
                                this.lastY = e.pageY;
                            }

                            handleMove: function(e) {
                                var textArea = e.target;
                                var top = textArea.scrollTop <= 0;
                                var bottom = textArea.scrollTop + textArea.clientHeight >= textArea.scrollHeight;
                                var up = e.pageY > this.lastY;
                                var down = e.pageY < this.lastY;

                                this.lastY = e.pageY;

                                // default (mobile safari) action when dragging past the top or bottom of a scrollable
                                // textarea is to scroll the containing div, so prevent that.
                                if((top && up) || (bottom && down)) {
                                    e.preventDefault();
                                    e.stopPropagation(); // this tops scroll going to parent
                                }

                                // Sencha disables textarea scrolling on iOS by default,
                                // so stop propagating the event to delegate to iOS.
                                if(!(top && bottom)) {
                                    e.stopPropagation(); // this tops scroll going to parent
                                }
                            }
                            */
                        },
                        event: 'painted'
                    },
                    {
                        fn: function(component, eOpts) {
                            /*
                            this.down('#textarea').on('keyup', this.grow, this);
                            this.textarea = this.element.down('textarea');
                            this.textarea.dom.style['overflow'] = 'hidden';


                            grow: function() {
                            this.textarea.setHeight(this.textarea.dom.scrollHeight); // scrollHeight is height of all the content
                            this.getScrollable().getScroller().scrollToEnd();
                            }
                            */
                        },
                        event: 'initialize'
                    }
                ]
            },
            {
                xtype: 'spacer',
                height: 10
            },
            {
                xtype: 'label',
                cls: 'blackFont',
                html: '2. Objectives: What are your goals? What you like to accomplish in this transaction or in dealing with this Challenge?'
            },
            {
                xtype: 'textareafield',
                id: 'nwCheckListQ12',
                itemId: 'mytextarea1',
                clearIcon: false,
                label: ''
            },
            {
                xtype: 'spacer',
                height: 10
            },
            {
                xtype: 'label',
                cls: 'blackFont',
                html: '3. PERSUASION PRECEDENTS : Transactions that can influence the discussion or provide a model for guidance Competitive Pricing/Terms. Examples of results from similar situations For other side, consider how you can counter their precedent.<br/>Precedents: Past experienced or outcomes that can influence the discussion of affect strategy.'
            },
            {
                xtype: 'textareafield',
                id: 'nwCheckListQ13',
                itemId: 'mytextarea2',
                clearIcon: false,
                label: ''
            },
            {
                xtype: 'spacer',
                height: 10
            },
            {
                xtype: 'label',
                cls: 'blackFont',
                html: '4. INTERESTS : What objectives or desires does the other side have beyond their stated positions? What do they want – need – that you might be able to address?'
            },
            {
                xtype: 'textareafield',
                id: 'nwCheckListQ14',
                itemId: 'mytextarea3',
                clearIcon: false,
                label: ''
            },
            {
                xtype: 'spacer',
                height: 10
            },
            {
                xtype: 'label',
                cls: 'blackFont',
                html: '5. ALTERNATIVES :  What are the various outcomes you want to consider? To what degree do they satisfy your objectives? State your HIGHEST GOAL, WALKAWAY and other alternatives.<br/> What are their options if the other party chooses not to work it out with you?'
            },
            {
                xtype: 'textareafield',
                id: 'nwCheckListQ15',
                itemId: 'mytextarea4',
                clearIcon: false,
                label: ''
            },
            {
                xtype: 'spacer',
                height: 10
            },
            {
                xtype: 'label',
                cls: 'blackFont',
                html: '6. TEAM : Are you doing this alone or with others? What is everyone’s role? <br/>What do you know about the other side’s participants? Biographical information? Authority? Personalities?'
            },
            {
                xtype: 'textareafield',
                id: 'nwCheckListQ16',
                itemId: 'mytextarea5',
                clearIcon: false,
                label: ''
            },
            {
                xtype: 'spacer',
                height: 10
            },
            {
                xtype: 'label',
                cls: 'blackFont',
                html: '7. STRATEGY/NEXT STEPS : What is your plan? What steps does it involve? When and how will you probe?'
            },
            {
                xtype: 'textareafield',
                id: 'nwCheckListQ17',
                itemId: 'mytextarea6',
                clearIcon: false,
                label: ''
            },
            {
                xtype: 'spacer',
                height: 10
            },
            {
                xtype: 'label',
                cls: 'blackFont',
                html: '8. TIMELINE : Lay out the time period during which you want to accomplish your objectives. When can you be expected to accomplish the steps in the process outlined in your strategy?'
            },
            {
                xtype: 'textareafield',
                id: 'nwCheckListQ18',
                itemId: 'mytextarea7',
                clearIcon: false,
                label: ''
            },
            {
                xtype: 'spacer',
                height: 10
            },
            {
                xtype: 'label',
                cls: 'blackFont',
                html: '9. SCRIPT : Write out the message or proposal you want to make. Include probing questions and hypotheticals. Share it with a team member or associate acting as your devil’s advocate. Once you are satisfied, build confidence in delivering the hard message with practice.'
            },
            {
                xtype: 'textareafield',
                id: 'nwCheckListQ19',
                itemId: 'mytextarea8',
                clearIcon: false,
                label: ''
            },
            {
                xtype: 'spacer',
                height: 10
            },
            {
                xtype: 'label',
                cls: 'blackFont',
                html: '10. UPDATE'
            },
            {
                xtype: 'textareafield',
                id: 'nwCheckListQ20',
                itemId: 'mytextarea9',
                clearIcon: false,
                label: ''
            },
            {
                xtype: 'spacer',
                height: 10
            }
        ],
        listeners: [
            {
                fn: 'onNwCheckListQ1Keyup1',
                event: 'keyup',
                delegate: '#nwCheckListQ11'
            },
            {
                fn: 'onNwCheckListQ2Focus1',
                event: 'focus',
                delegate: '#nwCheckListQ12'
            },
            {
                fn: 'onNwCheckListQ2Keyup1',
                event: 'keyup',
                delegate: '#nwCheckListQ12'
            },
            {
                fn: 'onNwCheckListQ3Keyup1',
                event: 'keyup',
                delegate: '#nwCheckListQ13'
            },
            {
                fn: 'onNwCheckListQ4Keyup1',
                event: 'keyup',
                delegate: '#nwCheckListQ14'
            },
            {
                fn: 'onNwCheckListQ5Keyup1',
                event: 'keyup',
                delegate: '#nwCheckListQ15'
            },
            {
                fn: 'onNwCheckListQ6Keyup1',
                event: 'keyup',
                delegate: '#nwCheckListQ16'
            },
            {
                fn: 'onNwCheckListQ7Keyup1',
                event: 'keyup',
                delegate: '#nwCheckListQ17'
            },
            {
                fn: 'onNwCheckListQ8Keyup1',
                event: 'keyup',
                delegate: '#nwCheckListQ18'
            },
            {
                fn: 'onNwCheckListQ9Keyup1',
                event: 'keyup',
                delegate: '#nwCheckListQ19'
            },
            {
                fn: 'onNwCheckListQ10Keyup1',
                event: 'keyup',
                delegate: '#nwCheckListQ20'
            },
            {
                fn: 'onMypanel2Painted1',
                event: 'painted'
            }
        ]
    },

    onNwCheckListQ1Keyup1: function(textfield, e, eOpts) {
        //console.log(textfield);
        /*
        var numOfRows=Ext.getCmp(textfield.id).getValue().split("\n").length;

        if( numOfRows>=4)
        {
        numOfRows= numOfRows++;
        Ext.getCmp(textfield.id).setMaxRows( numOfRows );
        }
        */
    },

    onNwCheckListQ2Focus1: function(textfield, e, eOpts) {
        /*
        //Sencha Touch 2.0 - How to set scrolling inside a textarea for Mobile Safari?
        handleTouch: function(e) {
            this.lastY = e.pageY;
        }

        handleMove: function(e) {
            var textArea = e.target;
            var top = textArea.scrollTop <= 0;
            var bottom = textArea.scrollTop + textArea.clientHeight >= textArea.scrollHeight;
            var up = e.pageY > this.lastY;
            var down = e.pageY < this.lastY;

            this.lastY = e.pageY;

            // default (mobile safari) action when dragging past the top or bottom of a scrollable
            // textarea is to scroll the containing div, so prevent that.
            if((top && up) || (bottom && down)) {
                e.preventDefault();
                e.stopPropagation(); // this tops scroll going to parent
            }

            // Sencha disables textarea scrolling on iOS by default,
            // so stop propagating the event to delegate to iOS.
            if(!(top && bottom)) {
                e.stopPropagation(); // this tops scroll going to parent
            }
        }
        */
    },

    onNwCheckListQ2Keyup1: function(textfield, e, eOpts) {
        /*
        var numOfRows=Ext.getCmp(textfield.id).getValue().split("\n").length;

        if( numOfRows>=4)
        {
            numOfRows= numOfRows++;
            Ext.getCmp(textfield.id).setMaxRows( numOfRows );
        }
        */
    },

    onNwCheckListQ3Keyup1: function(textfield, e, eOpts) {
        /*
        var numOfRows=Ext.getCmp(textfield.id).getValue().split("\n").length;

        if( numOfRows>=4)
        {
            numOfRows= numOfRows++;
            Ext.getCmp(textfield.id).setMaxRows( numOfRows );
        }
        */
    },

    onNwCheckListQ4Keyup1: function(textfield, e, eOpts) {
        /*
        var numOfRows=Ext.getCmp(textfield.id).getValue().split("\n").length;

        if( numOfRows>=4)
        {
            numOfRows= numOfRows++;
            Ext.getCmp(textfield.id).setMaxRows( numOfRows );
        }
        */
    },

    onNwCheckListQ5Keyup1: function(textfield, e, eOpts) {
        /*
        var numOfRows=Ext.getCmp(textfield.id).getValue().split("\n").length;

        if( numOfRows>=4)
        {
            numOfRows= numOfRows++;
            Ext.getCmp(textfield.id).setMaxRows( numOfRows );
        }
        */
    },

    onNwCheckListQ6Keyup1: function(textfield, e, eOpts) {
        /*
        var numOfRows=Ext.getCmp(textfield.id).getValue().split("\n").length;

        if( numOfRows>=4)
        {
            numOfRows= numOfRows++;
            Ext.getCmp(textfield.id).setMaxRows( numOfRows );
        }
        */
    },

    onNwCheckListQ7Keyup1: function(textfield, e, eOpts) {
        /*
        var numOfRows=Ext.getCmp(textfield.id).getValue().split("\n").length;

        if( numOfRows>=4)
        {
            numOfRows= numOfRows++;
            Ext.getCmp(textfield.id).setMaxRows( numOfRows );
        }
        */
    },

    onNwCheckListQ8Keyup1: function(textfield, e, eOpts) {
        /*
        var numOfRows=Ext.getCmp(textfield.id).getValue().split("\n").length;

        if( numOfRows>=4)
        {
            numOfRows= numOfRows++;
            Ext.getCmp(textfield.id).setMaxRows( numOfRows );
        }
        */
    },

    onNwCheckListQ9Keyup1: function(textfield, e, eOpts) {
        /*
        var numOfRows=Ext.getCmp(textfield.id).getValue().split("\n").length;

        if( numOfRows>=4)
        {
            numOfRows= numOfRows++;
            Ext.getCmp(textfield.id).setMaxRows( numOfRows );
        }
        */
    },

    onNwCheckListQ10Keyup1: function(textfield, e, eOpts) {
        /*
        var numOfRows=Ext.getCmp(textfield.id).getValue().split("\n").length;

        if( numOfRows>=4)
        {
            numOfRows= numOfRows++;
            Ext.getCmp(textfield.id).setMaxRows( numOfRows );
        }
        */
    },

    onMypanel2Painted1: function(element, eOpts) {
        Ext.getCmp('chkDataPnl').getScrollable().getScroller().scrollTo(0, 0, false);
        //alert();
        //console.log(Ext.getCmp('chkDataPnl').getScrollable());
    }

});