/*
 * File: app/store/probeAudioStore.js
 *
 * This file was generated by Sencha Architect version 3.0.4.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Sencha Touch 2.2.x library, under independent license.
 * License of Sencha Architect does not include license for Sencha Touch 2.2.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('MyApp.store.probeAudioStore', {
    extend: 'Ext.data.Store',

    requires: [
        'MyApp.model.prepAudioModel'
    ],

    config: {
        autoLoad: true,
        data: [
            {
                id: 9,
                title: 'Probe - Intro',
                discription: 'Discription....',
                //audioUrl : 'resourse/ProbeAudio/1-Probe-Intro.mp3'
                audioUrl: 'http://files.parse.com/89dfdb92-c71b-4192-9887-4e2bc6f85bc0/9c618ce4-c72f-44ee-ab22-2d21b141ab99-1-Probe-Intro.mp3'
            },
            {
                id: 10,
                title: 'Probe - What is Important to You',
                discription: 'Discription....',
                //audioUrl : 'resourse/ProbeAudio/2-Probe-What_is_Important_to_You.mp3'
                audioUrl: 'http://files.parse.com/89dfdb92-c71b-4192-9887-4e2bc6f85bc0/9979d485-bb0e-4786-9f6f-ece2bf2ff7c2-2-Probe-What_is_Important_to_You.mp3'
            },
            {
                id: 11,
                title: 'Probe - Hypotheticals',
                discription: 'Discription....',
                // audioUrl : 'resourse/ProbeAudio/3-Probe-Hypotheticals.mp3'
                audioUrl: 'http://files.parse.com/89dfdb92-c71b-4192-9887-4e2bc6f85bc0/98207c46-1344-44d8-99be-253da7185fea-3-Probe-Hypotheticals.mp3'
            },
            {
                id: 12,
                title: 'Probe - Answering Questions with Questions',
                discription: 'Discription....',
                // audioUrl : 'resourse/ProbeAudio/4-Probe-Answering_Questions_with_Questions.mp3'
                audioUrl: 'http://files.parse.com/89dfdb92-c71b-4192-9887-4e2bc6f85bc0/32509d2a-b9e8-4dfe-9e1f-372914cb27c7-4-Probe-Answering_Questions_with_Questions.mp3'
            },
            {
                id: 13,
                title: 'Probe - Tell Me More',
                discription: 'Discription....',
                // audioUrl : 'resourse/ProbeAudio/5-Probe-Tell_Me_More.mp3'
                audioUrl: 'http://files.parse.com/89dfdb92-c71b-4192-9887-4e2bc6f85bc0/8527ff64-d0ad-4f14-b1ec-d921f4df78a9-5-Probe-Tell_Me_More.mp3'
            }
        ],
        model: 'MyApp.model.prepAudioModel',
        storeId: 'probeAudioStore'
    }
});