/*
 * File: app/store/preprationAudioStore.js
 *
 * This file was generated by Sencha Architect version 3.0.4.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Sencha Touch 2.2.x library, under independent license.
 * License of Sencha Architect does not include license for Sencha Touch 2.2.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('MyApp.store.preprationAudioStore', {
    extend: 'Ext.data.Store',

    requires: [
        'MyApp.model.prepAudioModel'
    ],

    config: {
        autoLoad: true,
        data: [
            {
                id: 1,
                title: 'Preparation- Intro',
                discription: 'Discription....',
                //audioUrl : 'resourse/PrepareAudio/1-Preparation-Intro.mp3'
                audioUrl: 'http://files.parse.com/89dfdb92-c71b-4192-9887-4e2bc6f85bc0/da2e43b6-510e-48de-981d-1e53bd961191-1-Preparation-Intro_24April-214.mp3'//audioUrl : 'http://files.parse.com/89dfdb92-c71b-4192-9887-4e2bc6f85bc0/9afc9397-333b-46f1-8d45-c959a2f60b7c-1_Preparation_Intro.mp3'
                //audioUrl : 'http://files.parse.com/89dfdb92-c71b-4192-9887-4e2bc6f85bc0/930a4de8-99a4-402b-967a-ea1d2665d915-1-Preparation-Intro.mp3'
            },
            {
                id: 2,
                title: 'Preparation - Precedents',
                discription: 'Discription....',
                //audioUrl : 'resourse/PrepareAudio/2-Preparation-Precedents.mp3'
                audioUrl: 'http://files.parse.com/89dfdb92-c71b-4192-9887-4e2bc6f85bc0/256973cf-f314-4657-a4b5-be51a5ba2c08-2-Preparation-Precedents.mp3'
            },
            {
                id: 3,
                title: 'Preparation - Alternatives',
                discription: 'Discription....',
                //audioUrl : 'resourse/PrepareAudio/3-Preparation-Alternatives.mp3'
                audioUrl: 'http://files.parse.com/89dfdb92-c71b-4192-9887-4e2bc6f85bc0/51ddc1d4-2b50-4d9a-9ef2-85d38e47de44-3-Preparation-Alternatives.mp3'
            },
            {
                id: 4,
                title: 'Preparation - Interests',
                discription: 'Discription....',
                //audioUrl : 'resourse/PrepareAudio/4-Preparation-Interests.mp3'
                audioUrl: 'http://files.parse.com/89dfdb92-c71b-4192-9887-4e2bc6f85bc0/a1680fc3-f0cc-4b38-80d0-0b4441cc2263-4-Preparation-Interests.mp3'
            },
            {
                id: 5,
                title: 'Preparation - Deadlines',
                discription: 'Discription....',
                //audioUrl : 'resourse/PrepareAudio/5-Preparation-Deadlines.mp3'
                audioUrl: 'http://files.parse.com/89dfdb92-c71b-4192-9887-4e2bc6f85bc0/f4903e86-1d0b-4190-9eaf-608b40524cde-5-Preparation-Deadlines.mp3'
            },
            {
                id: 6,
                title: 'Preparation - Highest Goal and Walkaway',
                discription: 'Discription....',
                //audioUrl : 'resourse/PrepareAudio/6-Preparation-Highest_Goal_and_Walkaway.mp3'
                audioUrl: 'http://files.parse.com/89dfdb92-c71b-4192-9887-4e2bc6f85bc0/d646e9ee-ce3d-4606-b422-325464aafa75-6-Preparation-Highest_Goal_and_Walkaway.mp3'
            },
            {
                id: 7,
                title: 'Preparation - Strengths and Weaknesses',
                discription: 'Discription....',
                //audioUrl : 'resourse/PrepareAudio/7-Preparation-Strengths_and_Weaknesses.mp3'
                audioUrl: 'http://files.parse.com/89dfdb92-c71b-4192-9887-4e2bc6f85bc0/114de3e9-ce7d-4bf8-9d66-262de099fc91-7-Preparation-Strengths_and_Weaknesses.mp3'
            },
            {
                id: 8,
                title: 'Preparation - Strategy and Team',
                discription: 'Discription....',
                //audioUrl : 'resourse/PrepareAudio/8-Preparation-Strategy_and_Team.mp3'
                audioUrl: 'http://files.parse.com/89dfdb92-c71b-4192-9887-4e2bc6f85bc0/ce4023de-aea8-4034-a658-c063ca640808-8-Preparation-Strategy_and_Team.mp3'
            }
        ],
        model: 'MyApp.model.prepAudioModel',
        storeId: 'preprationAudioStore'
    }
});